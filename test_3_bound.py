import numpy as np
import matplotlib.pyplot as plt
from diff_lib import *

correct_func_input = "131/501/x+1039/5010*x-1/2"
func_p_input = "1/x"
func_q_input = "-1/x/x"
func_f_input = "0.5/x/x"
sigma_1, gamma_1, delta_1 = 2, -3, 0.1
sigma_2, gamma_2, delta_2 = 1, 0, 1.6
a = 1
b = 10
n1 = 25
n2 = 100

(x_corr, y_corr) = calculate_func_x(lambda x: eval(correct_func_input), a, 25, b)
(x1_arr, y1_arr) = boundary_solve(lambda x: eval(func_p_input),
                                  lambda x: eval(func_q_input),
                                  lambda x: eval(func_f_input),
                                  sigma_1, gamma_1, delta_1,
                                  sigma_2, gamma_2, delta_2,
                                  n1, a, b)

(x2_arr, y2_arr) = boundary_solve(lambda x: eval(func_p_input),
                                  lambda x: eval(func_q_input),
                                  lambda x: eval(func_f_input),
                                  sigma_1, gamma_1, delta_1,
                                  sigma_2, gamma_2, delta_2,
                                  n2, a, b)

plt.title(f"Метод конечных разностей и метод прогонки\nдля решения краевой задачи второго порядка (n={n1})")
plt.xlabel("X")
plt.ylabel("Y")


plt.plot(x1_arr, y1_arr, 'r')
plt.plot(x_corr, y_corr, 'b.')

plt.show()

plt.title(f"Метод конечных разностей и метод прогонки\nдля решения краевой задачи второго порядка (n={n2})")
plt.xlabel("X")
plt.ylabel("Y")


plt.plot(x2_arr, y2_arr, 'r')
plt.plot(x_corr, y_corr, 'b.')

plt.show()
