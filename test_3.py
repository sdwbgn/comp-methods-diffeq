import numpy as np
import matplotlib.pyplot as plt
from math import *
from diff_lib import *


correct_solution = "-x * x + 2 * x - 2 + 12 * exp(-x)"
func_input = "-y - x * x"
x_0 = 0
y_0 = 10
x_max = 100
n = 48

plt.title("Метод Рунге-Кутта 2 порядка")
plt.xlabel("X")
plt.ylabel("Y")

plt.plot(*runge_kutta_2(lambda x, y: eval(func_input),
                        x_0, y_0, n, x_max), 'r')

plt.plot(*calculate_func_x(lambda x: eval(correct_solution),
                           x_0, n, x_max), 'b.')

plt.show()


plt.title("Метод Рунге-Кутта 4 порядка")
plt.xlabel("X")
plt.ylabel("Y")

plt.plot(*runge_kutta_4(lambda x, y: eval(func_input),
                        x_0, y_0, n, x_max), 'r')

plt.plot(*calculate_func_x(lambda x: eval(correct_solution),
                           x_0, n, x_max), 'b.')

plt.show()