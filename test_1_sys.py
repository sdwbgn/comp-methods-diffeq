import numpy as np
import matplotlib.pyplot as plt
from diff_lib import *


sys_func1_input = "log(2 * x + sqrt(4 * x * x + v * v))"
sys_func2_input = "sqrt(4 * x * x + u * u)"
x_0 = 0
u_0 = 0.5
v_0 = 1
x_max = 10
n = 100

plt.title("Метод Рунге-Кутта 4 порядка для систем дифф. уравнений")
plt.xlabel("X")
plt.ylabel("Y")

(x_arr, u_arr, v_arr) = runge_kutta_sys_4(lambda x, u, v: eval(sys_func1_input),
                                          lambda x, u, v: eval(sys_func2_input),
                                          x_0, u_0, v_0, n, x_max)

plt.plot(x_arr, u_arr, 'r')
plt.plot(x_arr, v_arr, 'g')


plt.show()