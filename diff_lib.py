from typing import *
import numpy as np
import matplotlib.pyplot as plt
from math import *


def calculate_func_x(func: Callable[[float], float],
                     x_0: float,
                     n: int,
                     x_max: float) -> (np.ndarray, np.ndarray):
    # Функция составляющая сеточную функцию из заданной
    x_arr = np.array([])
    y_arr = np.array([])
    x_h = (x_max - x_0) / n
    x_cur = x_0
    for i in range(n):
        x_arr = np.append(x_arr, x_cur)
        y_arr = np.append(y_arr, func(x_cur))
        x_cur += x_h
    return x_arr, y_arr


def runge_kutta_4(func: Callable[[float, float], float],
                  x_0: float,
                  y_0: float,
                  n: int,
                  x_max: float) -> (np.ndarray, np.ndarray):
    # Метод Рунге-Кутта 4-го порядка

    # Подготовим сетку
    x_arr = np.array([x_0])
    y_arr = np.array([y_0])

    # Обозначим предыдущий x и y
    x_prev = x_0
    y_prev = y_0

    # Вычислим h и текущий x
    x_h = (x_max - x_0) / n
    x_cur = x_0 + x_h

    # Процесс
    for i in range(n):
        # Вычислим k_1 .. k_4
        k_1 = func(x_prev, y_prev)
        k_2 = func(x_prev + x_h/2, y_prev + (x_h/2) * k_1)
        k_3 = func(x_prev + x_h/2, y_prev + (x_h/2) * k_2)
        k_4 = func(x_prev + x_h, y_prev + x_h * k_3)

        # Подставим k в формулу
        y_cur = y_prev + (x_h/6)*(k_1 + 2 * k_2 + 2 * k_3 + k_4)
        
        # Обновим ответ
        x_arr = np.append(x_arr, x_cur)
        y_arr = np.append(y_arr, y_cur)
        
        # Запомним текущие значения
        x_prev = x_cur
        y_prev = y_cur
        
        # Добавим x_h
        x_cur += x_h
    return x_arr, y_arr


def runge_kutta_2(func: Callable[[float, float], float],
                  x_0: float,
                  y_0: float,
                  n: int,
                  x_max: float) -> (np.ndarray, np.ndarray):
    # Метод Рунге-Кутта 2-го порядка

    # Подготовим сетку
    x_arr = np.array([x_0])
    y_arr = np.array([y_0])

    # Обозначим предыдущий x и y
    x_prev = x_0
    y_prev = y_0

    # Вычислим h и текущий x
    x_h = (x_max - x_0) / n
    x_cur = x_0 + x_h

    # Процесс
    for i in range(n):
        # Вычислим k_1 .. k_2
        k_1 = func(x_prev, y_prev)
        k_2 = func(x_prev + x_h, y_prev + x_h * k_1)

        # Подставим k в формулу
        y_cur = y_prev + (x_h/2)*(k_1 + k_2)
        
        # Обновим ответ
        x_arr = np.append(x_arr, x_cur)
        y_arr = np.append(y_arr, y_cur)
        
        # Запомним текущие значения
        x_prev = x_cur
        y_prev = y_cur
        
        # Добавим x_h
        x_cur += x_h
    return x_arr, y_arr


def runge_kutta_sys_4(func1: Callable[[float, float, float], float],
                      func2: Callable[[float, float, float], float],
                      x_0: float,
                      y_1_0: float,
                      y_2_0: float,
                      n: int,
                      x_max: float) -> (np.ndarray, np.ndarray):
    # Метод Рунге-Кутта 4-го порядка для систем из 2 уравнений

    # Подготовим сетку
    x_arr = np.array([x_0])
    y_1_arr = np.array([y_1_0])
    y_2_arr = np.array([y_2_0])

    # Обозначим предыдущий x, y_1 и y_2
    x_prev = x_0
    y_1_prev = y_1_0
    y_2_prev = y_2_0

    # Вычислим h и текущий x
    x_h = (x_max - x_0) / n
    x_cur = x_0 + x_h

    # Процесс
    for i in range(n):
        # Вычислим k_1_1 .. k_1_4 и k_2_1 .. k_2_4
        k_1_1 = func1(x_prev, y_1_prev, y_2_prev)
        k_2_1 = func2(x_prev, y_1_prev, y_2_prev)
        k_1_2 = func1(x_prev + x_h / 2, y_1_prev +
                      (x_h / 2) * k_1_1, y_2_prev + (x_h / 2) * k_2_1)
        k_2_2 = func2(x_prev + x_h / 2, y_1_prev +
                      (x_h / 2) * k_1_1, y_2_prev + (x_h / 2) * k_2_1)
        k_1_3 = func1(x_prev + x_h / 2, y_1_prev +
                      (x_h / 2) * k_1_2, y_2_prev + (x_h / 2) * k_2_2)
        k_2_3 = func2(x_prev + x_h / 2, y_1_prev +
                      (x_h / 2) * k_1_2, y_2_prev + (x_h / 2) * k_2_2)
        k_1_4 = func1(x_prev + x_h, y_1_prev +
                      x_h * k_1_3, y_2_prev + x_h * k_2_3)
        k_2_4 = func2(x_prev + x_h, y_1_prev +
                      x_h * k_1_3, y_2_prev + x_h * k_2_3)

        # Подставим k в формулу
        y_1_cur = y_1_prev + (x_h / 6) * (k_1_1 + 2 * k_1_2 + 2 * k_1_3 + k_1_4)
        y_2_cur = y_2_prev + (x_h / 6) * (k_2_1 + 2 * k_2_2 + 2 * k_2_3 + k_2_4)
        
        # Обновим ответ
        x_arr = np.append(x_arr, x_cur)
        y_1_arr = np.append(y_1_arr, y_1_cur)
        y_2_arr = np.append(y_2_arr, y_2_cur)
        
        # Запомним текущие значения
        x_prev = x_cur
        y_1_prev = y_1_cur
        y_2_prev = y_2_cur

        x_cur += x_h
    return x_arr, y_1_arr, y_2_arr


def boundary_solve(func_p: Callable[[float], float],
                   func_q: Callable[[float], float],
                   func_f: Callable[[float], float],
                   sigma_1: float, gamma_1: float, delta_1: float,
                   sigma_2: float, gamma_2: float, delta_2: float,
                   n: int, x_0: float, x_n: float) -> (np.ndarray, np.ndarray):
    # Вычислим h
    x_h = (x_n - x_0) / n

    # Вычислим A_i
    a = [0.0]
    for i in range(1, n):
        a.append(1 - x_h * func_p(x_0 + x_h * i) / 2)
    a.append(-gamma_2)

    # Вычислим C_i
    c = [x_h * sigma_1 - gamma_1]
    for i in range(1, n):
        c.append(x_h * x_h * func_q(x_0 + x_h * i) - 2)
    c.append(x_h * sigma_2 + gamma_2)

    # Вычислим B_i
    b = [gamma_1]
    for i in range(1, n):
        b.append(1 + x_h * func_p(x_0 + x_h * i) / 2)
    b.append(0)

    # Вычислим F_i
    f = [x_h * delta_1]
    for i in range(1, n):
        f.append(x_h * x_h * func_f(x_0 + i * x_h))
    f.append(x_h * delta_2)

    # Вычислим alpha_i
    alpha = [-b[0] / c[0]]
    for i in range(1, n + 1):
        alpha.append(-b[i] / (c[i] + a[i] * alpha[i - 1]))

    # Вычислим beta_i
    beta = [f[0] / c[0]]
    for i in range(1, n+1):
        beta.append((f[i] - a[i] * beta[i-1]) / (c[i] + a[i] * alpha[i-1]))

    # Подготовим сетку
    x_arr = [x_0 + i * x_h for i in range(n + 1)]
    y_arr = [0.0 for i in range(n+1)]

    # Вычисление y_i методом прогонки
    y_arr[n] = beta[n]
    for i in range(n-1, -1, -1):
        y_arr[i] = beta[i] + alpha[i] * y_arr[i + 1]

    return np.array(x_arr), np.array(y_arr)
