import numpy as np
import matplotlib.pyplot as plt
from diff_lib import *


sys_func1_input = "x + v * v"
sys_func2_input = "x * u"
x_0 = 0
u_0 = 1
v_0 = -1
x_max = 2
n = 30

plt.title("Метод Рунге-Кутта 4 порядка для систем дифф. уравнений")
plt.xlabel("X")
plt.ylabel("Y")

(x_arr, u_arr, v_arr) = runge_kutta_sys_4(lambda x, u, v: eval(sys_func1_input),
                                          lambda x, u, v: eval(sys_func2_input),
                                          x_0, u_0, v_0, n, x_max)

plt.plot(x_arr, u_arr, 'r')
plt.plot(x_arr, v_arr, 'g')


plt.show()