import numpy as np
import matplotlib.pyplot as plt
from math import *
from diff_lib import *


correct_solution = "4 - x - 4 * exp(-x)"
func_input = "3 - y - x"
x_0 = 0
y_0 = 0
x_max = 30
n = 30

plt.title("Метод Рунге-Кутта 2 порядка")
plt.xlabel("X")
plt.ylabel("Y")

plt.plot(*runge_kutta_2(lambda x, y: eval(func_input),
                        x_0, y_0, n, x_max), 'r')

plt.plot(*calculate_func_x(lambda x: eval(correct_solution),
                           x_0, n, x_max), 'b.')

plt.show()


plt.title("Метод Рунге-Кутта 4 порядка")
plt.xlabel("X")
plt.ylabel("Y")

plt.plot(*runge_kutta_4(lambda x, y: eval(func_input),
                        x_0, y_0, n, x_max), 'r')

plt.plot(*calculate_func_x(lambda x: eval(correct_solution),
                           x_0, n, x_max), 'b.')

plt.show()