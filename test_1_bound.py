import numpy as np
import matplotlib.pyplot as plt
from diff_lib import *

correct_func_input = "-1/1350*exp(-3/2*x)/(4*exp(13/5)*exp(-3/2)+3*exp(-39/20)*exp(2))*(940*exp(13/5)-9393*exp(2))+2/675*exp(2*x)*(3131*exp(-3/2)+235*exp(-39/20))/(4*exp(13/5)*exp(-3/2)+3*exp(-39/20)*exp(2))-2/3*x**2+2/9*x-13/27"
func_p_input = "- 1 / 2"
func_q_input = "-3"
func_f_input = "2 * x * x"
sigma_1, gamma_1, delta_1 = 1, -2, 0.6
sigma_2, gamma_2, delta_2 = 1, 0, 1
a = 1
b = 1.3
n1 = 25
n2 = 100

(x_corr, y_corr) = calculate_func_x(lambda x: eval(correct_func_input), a, 25, b)
(x1_arr, y1_arr) = boundary_solve(lambda x: eval(func_p_input),
                                  lambda x: eval(func_q_input),
                                  lambda x: eval(func_f_input),
                                  sigma_1, gamma_1, delta_1,
                                  sigma_2, gamma_2, delta_2,
                                  n1, a, b)

(x2_arr, y2_arr) = boundary_solve(lambda x: eval(func_p_input),
                                  lambda x: eval(func_q_input),
                                  lambda x: eval(func_f_input),
                                  sigma_1, gamma_1, delta_1,
                                  sigma_2, gamma_2, delta_2,
                                  n2, a, b)

plt.title(f"Метод конечных разностей и метод прогонки\nдля решения краевой задачи второго порядка (n={n1})")
plt.xlabel("X")
plt.ylabel("Y")


plt.plot(x1_arr, y1_arr, 'r')
plt.plot(x_corr, y_corr, 'b.')

plt.show()

plt.title(f"Метод конечных разностей и метод прогонки\nдля решения краевой задачи второго порядка (n={n2})")
plt.xlabel("X")
plt.ylabel("Y")


plt.plot(x2_arr, y2_arr, 'r')
plt.plot(x_corr, y_corr, 'b.')

plt.show()
